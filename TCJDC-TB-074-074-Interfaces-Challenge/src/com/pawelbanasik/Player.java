package com.pawelbanasik;

import java.util.ArrayList;
import java.util.List;

public class Player implements ISaveable {

	private String name;
	private int hitPoints;
	private int strength;
	private String weapon;

	protected Player(String name, int hitPoints, int strength) {
		this.name = name;
		this.hitPoints = hitPoints;
		this.strength = strength;
		this.weapon = "Sword";
	}

	@Override
	public List<String> write() {
		List<String> list = new ArrayList<>();
		list.add(0, name);
		list.add(1, "" + hitPoints);
		list.add(2, "" + strength);
		list.add(3, weapon);

		return list;
	}

	@Override
	public void read(List<String> savedValues) {
		if (savedValues != null && savedValues.size() > 0) {
			name = savedValues.get(0);
			hitPoints = Integer.parseInt(savedValues.get(1));
			strength = Integer.parseInt(savedValues.get(2));
			weapon = savedValues.get(3);
		}
		
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHitPoints() {
		return hitPoints;
	}

	public void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public String getWeapon() {
		return weapon;
	}

	public void setWeapon(String weapon) {
		this.weapon = weapon;
	}

	@Override
	public String toString() {
		return name + " " + hitPoints + " " + strength + " " + weapon;
	}

}
